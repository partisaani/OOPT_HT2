/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopt_ht2;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author Simo
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button clearSelectionsButton;
    @FXML
    private WebView contentWebView;
    @FXML
    private MenuButton keywordDropdown;
    @FXML
    private Button openButton1;
    @FXML
    private Button openButton2;
    @FXML
    private ComboBox<Pattern> patternComboBox;
    @FXML
    private Button searchButton;
    @FXML
    private ListView<Pattern> top3ListView;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // open main page initially
        openWebPage("patterns/mainpage.html");
        
        // Factory
        Pattern factory = new Pattern("Factory", "Creational", "patterns/factory.html");
        factory.addKeyword("Create multiple objects");
        factory.addKeyword("Avoid code duplication");
        factory.addKeyword("Easier code maintenance");
        
        // Singleton
        Pattern singleton = new Pattern("Singleton", "Creational", "patterns/singleton.html");
        singleton.addKeyword("Only one instance for class");
        singleton.addKeyword("Global access point");

        // Adapter
        Pattern adapter = new Pattern("Adapter", "Creational", "patterns/adapter.html");
        adapter.addKeyword("Allows the programmer to reuse code");
        adapter.addKeyword("Convert interface to another");
        
        //Composite
        Pattern composite = new Pattern("Composite", "Structural", "patterns/composite.html");
        composite.addKeyword("Hierarchies");
        composite.addKeyword("Child components");
        composite.addKeyword("Tree-structure");

        //Decorator
        Pattern decorator = new Pattern("Decorator", "Structural", "patterns/decorator.html");
        decorator.addKeyword("Adds responsibilities dynamically");
        decorator.addKeyword("Flexible way of extending functionality");
        decorator.addKeyword("Lets you change the skin of an object");
        decorator.addKeyword("Have similar structure diagram than Composite pattern");
        
        // Command
        Pattern command = new Pattern("Command", "Structural", "patterns/command.html");
        command.addKeyword("Decouple action and actor");
        command.addKeyword("Method invocation encapsulation");
        command.addKeyword("Action abstraction");
        command.addKeyword("Modularity");

        // Proxy
        Pattern proxy = new Pattern("Proxy", "Structural", "patterns/proxy.html");
        proxy.addKeyword("Limit access to certain resources");
        proxy.addKeyword("Provide indirect access to class");

        // Facade
        Pattern facade = new Pattern("Facade", "Structural", "patterns/facade.html");
        facade.addKeyword("Simplify the client");
        facade.addKeyword("Easier code maintenance");
        facade.addKeyword("Hide system's complexity");

        //Template
        Pattern template = new Pattern("Template", "Behavioral", "patterns/templatemethod.html");
        template.addKeyword("Based on abstraction");
        template.addKeyword("Abstract a process for multiple different classes");
        template.addKeyword("Allows the programmer to reuse code");
        template.addKeyword("Provides a skeleton of an algorithm");
        
        //Observer
        Pattern observer = new Pattern("Observer", "Behavioral", "patterns/observer.html");
        observer.addKeyword("Monitoring state changes");
        observer.addKeyword("Push & Pull requests");
        observer.addKeyword("Used in GUIs");
        observer.addKeyword("Automation");

        // Iterator
        Pattern iterator = new Pattern("Iterator", "Behavioral", "patterns/iterator.html");
        iterator.addKeyword("Handling different kinds of collections");

        //Strategy
        Pattern strategy = new Pattern("Strategy", "Behavioral", "patterns/strategy.html");
        strategy.addKeyword("Like State pattern except in its intent");
        strategy.addKeyword("Like Template Method except in its granularity");
        strategy.addKeyword("Lets change the guts of an object");
        
        //State
        Pattern state = new Pattern("State", "Behavioral", "patterns/state.html");
        state.addKeyword("Solution of how to make behavior depending on state");
        state.addKeyword("Objects are often singletons");
        
        // Compound
        Pattern compound = new Pattern("Compound", "Behavioral", "patterns/compound.html");
        compound.addKeyword("Multiple patterns");
        compound.addKeyword("Joint application");
        compound.addKeyword("Pattern of patterns");
        compound.addKeyword("MVC");
        
        // add patterns to store
        PatternStore pstore = PatternStore.getInstance();
        pstore.addPattern(factory);
        pstore.addPattern(singleton);
        pstore.addPattern(adapter);
        pstore.addPattern(composite);
        pstore.addPattern(decorator);
        pstore.addPattern(command);
        pstore.addPattern(proxy);
        pstore.addPattern(facade);
        pstore.addPattern(template);
        pstore.addPattern(observer);
        pstore.addPattern(iterator);
        pstore.addPattern(strategy);
        pstore.addPattern(state);
        pstore.addPattern(compound);

        // add patterns to combobox
        for (Pattern p : pstore.getPatterns())
            patternComboBox.getItems().add(p);

        // add keywords to choicebox
        keywordDropdown.getItems().clear();
        ArrayList<String> allKeywords = pstore.getAllKeywords();
        Collections.sort(allKeywords);
        for (String s : allKeywords) {
            CheckBox c = new CheckBox(s);
            CustomMenuItem item = new CustomMenuItem(c);
            item.setText(s);
            item.setHideOnClick(false);
            keywordDropdown.getItems().add(item);
        }

    }

    @FXML
    private void clearSelectionsButtonClicked(ActionEvent event) {
        for (MenuItem n : keywordDropdown.getItems()) {
            CustomMenuItem item = (CustomMenuItem) n;
            CheckBox c = (CheckBox) item.getContent();
            c.setSelected(false);
        }
        top3ListView.getItems().clear();
    }

    private void openWebPage(String filename) {
        try {
            WebEngine engine = contentWebView.getEngine();
            String url = getClass().getResource(filename).toExternalForm();
            engine.load(url);
        } catch (Exception e) {
        }
    }

    @FXML
    private void openButton1Clicked(ActionEvent event) {
        Pattern selectedPattern = patternComboBox.getSelectionModel().getSelectedItem();
        if (selectedPattern != null) {
            openWebPage(selectedPattern.getFilename());
        }
    }

    @FXML
    private void openButton2Clicked(ActionEvent event) {
        Pattern selectedPattern = top3ListView.getSelectionModel().getSelectedItems().get(0);
        if (selectedPattern != null) {
            openWebPage(selectedPattern.getFilename());
        }
    }

    @FXML
    private void searchButtonClicked(ActionEvent event) {

        ArrayList<String> keywordsSelected = new ArrayList();

        // get selected keywords
        for (MenuItem n : keywordDropdown.getItems()) {
            CustomMenuItem item = (CustomMenuItem) n;
            CheckBox c = (CheckBox) item.getContent();
            if (c.isSelected()) {
                keywordsSelected.add(c.getText());
            }
        }
        
        // Check if any keywords have been selected
        if (keywordsSelected.isEmpty()) {
            return;
        }

        // Encapsulate patterns and their corresponding match scores
        // into PatternScore classes
        ArrayList<PatternScore> patternScoreList = new ArrayList();

        // Calculate keyword match score for each pattern
        for (Pattern p : PatternStore.getInstance().getPatterns()) {
            PatternScore ps = new PatternScore(p);
            for (String s : p.getKeywords()) {
                for (String kw : keywordsSelected) {
                    if (s.equals(kw)) {
                        ps.incrementScore();
                        break;
                    }
                }
            }
            patternScoreList.add(ps);
        }

        // Sort by score
        Collections.sort(patternScoreList);
        Collections.reverse(patternScoreList);

        // Add top 3 matches to UI
        top3ListView.getItems().clear();
        for (int i = 0; i < 3; i++) {
            if(patternScoreList.get(i).getScore() > 0){
                top3ListView.getItems().add(patternScoreList.get(i).getPattern());
            }   
        }
    }

}
