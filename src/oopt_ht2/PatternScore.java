/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopt_ht2;

/**
 *
 * @author lonssi
 */
public class PatternScore implements Comparable<PatternScore> {
    
    private Pattern pattern;
    private int score;

    public PatternScore(Pattern pattern) {
        this.pattern = pattern;
        this.score = 0;
    }
    
    public void incrementScore() {
        this.score = this.score + 1;
    }
    
    public int getScore() {
        return this.score;
    }
    
    public Pattern getPattern() {
        return this.pattern;
    }
    
    public int compareTo(PatternScore o) {
        return (this.score - o.getScore());
    }
    
}
